# Tutorials (Some Incomplete)
* EnrichmentMap
* CausalPath
* NetBoxR
* Mutex
* Mapping


## To be completed
* Partial Correlation Networks
* Perturbation Biology: Belief Propagation
* Diffusion

# Installation
This installs packages listed in r-requirements.txt
```
# Set up to use CRAN
setRepositories(ind=1:6)
if(is.null(getOption("repos"))) { options(repos="http://cran.rstudio.com/") }
if(!all(c("devtools", "stringr") %in% rownames(installed.packages()))) { install.packages(c("devtools", "stringr")) }

source("https://gist.githubusercontent.com/cannin/6b8c68e7db19c4902459/raw/installPackages.R")
installPackages("r-requirements.txt")

# Force install paxtoolsr development version
devtools::install_github("BioPAX/paxtoolsr")
```
